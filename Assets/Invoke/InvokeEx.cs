﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeEx : MonoBehaviour {


    public GameObject target;
    public bool repeat;

    void Start()
    {
        if (repeat)
        InvokeRepeating("SpawnObjectR", 2, 1);
        else
        Invoke("SpawnObject", 2);
    }

    void SpawnObject()
    {
     GameObject instan=   Instantiate(target, new Vector3(0, 2, 0), Quaternion.identity) as GameObject;
        instan.transform.SetParent(transform);
    }
    void SpawnObjectR()
    {
        float x = Random.Range(-2.0f, 2.0f);
        float z = Random.Range(-2.0f, 2.0f);
       GameObject go= Instantiate(target, new Vector3(x, 2, z), Quaternion.identity)as GameObject;
        go.transform.SetParent(transform);
    }
}
