﻿using UnityEngine;
using System.Collections;

public class KeyandButtons : MonoBehaviour
{
    public void Update()
    {
        #region KeyInput
        if (Input.GetKey(KeyCode.G))
        {
            Debug.Log("key is Pressed");
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("key IS Down");
        }
        if (Input.GetKeyUp(KeyCode.H))
        {
            Debug.Log("key IS Up");
        }
        #endregion  
        #region buttons
        if (Input.GetButton("Jump"))
        {
            Debug.Log("Jump Is Pressed");
        }
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Debug.Log("Horizontal=" + x);
        Debug.Log("Vertical=" + y);
        #endregion
        #region Mouse
        if (Input.GetMouseButton(0))
        {
            Debug.Log("Mouse is pressed");
        }
       

        Vector3 mousePosition = Input.mousePosition;
        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Debug.Log("Mouse Real Pos=" + mousePosition);
        Debug.Log("MouseWorldPosition=" + mouseWorldPosition);
        #endregion
    }
}
